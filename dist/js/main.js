var lennConfig = {
    "urls": [
        "lenn.opus",
        "audio/soundpacks/lenn/lenn.mp3",
        "lenn.webm"
    ],
    "sprite": {
        "lenn_boo10": [
            0,
            1556,
            false
        ],
        "lenn_boo11": [
            1805,
            791,
            false
        ],
        "lenn_boo12": [
            2846,
            1508,
            false
        ],
        "lenn_boo13": [
            4603,
            1496,
            false
        ],
        "lenn_boo14": [
            6349,
            1091,
            false
        ],
        "lenn_boo15": [
            7689,
            1455,
            false
        ],
        "lenn_boo16": [
            9393,
            1176,
            false
        ],
        "lenn_boo17": [
            10819,
            840,
            false
        ],
        "lenn_boo1": [
            11908,
            1653,
            false
        ],
        "lenn_boo2": [
            13811,
            1673,
            false
        ],
        "lenn_boo3": [
            15734,
            1033,
            false
        ],
        "lenn_boo4": [
            17016,
            1542,
            false
        ],
        "lenn_boo5": [
            18807,
            1399,
            false
        ],
        "lenn_boo6": [
            20455,
            1370,
            false
        ],
        "lenn_boo7": [
            22075,
            544,
            false
        ],
        "lenn_boo8": [
            22868,
            1247,
            false
        ],
        "lenn_boo9": [
            24365,
            1155,
            false
        ],
        "lenn_clap10": [
            25769,
            95,
            false
        ],
        "lenn_clap11": [
            26114,
            144,
            false
        ],
        "lenn_clap12": [
            26507,
            70,
            false
        ],
        "lenn_clap13": [
            26827,
            89,
            false
        ],
        "lenn_clap14": [
            27166,
            112,
            false
        ],
        "lenn_clap15": [
            27527,
            70,
            false
        ],
        "lenn_clap16": [
            27847,
            70,
            false
        ],
        "lenn_clap1": [
            28167,
            118,
            false
        ],
        "lenn_clap2": [
            28534,
            144,
            false
        ],
        "lenn_clap3": [
            28928,
            133,
            false
        ],
        "lenn_clap4": [
            29310,
            47,
            false
        ],
        "lenn_clap5": [
            29606,
            118,
            false
        ],
        "lenn_clap6": [
            29974,
            47,
            false
        ],
        "lenn_clap7": [
            30270,
            24,
            false
        ],
        "lenn_clap8": [
            30544,
            24,
            false
        ],
        "lenn_clap9": [
            30817,
            47,
            false
        ],
        "lenn_cry1": [
            31113,
            3618,
            false
        ],
        "lenn_cry2": [
            34980,
            2085,
            false
        ],
        "lenn_drink1": [
            37315,
            1810,
            false
        ],
        "lenn_gg1": [
            39375,
            1254,
            false
        ],
        "lenn_gg2": [
            40879,
            1675,
            false
        ],
        "lenn_gg3": [
            42803,
            1094,
            false
        ],
        "lenn_go1": [
            44146,
            2094,
            false
        ],
        "lenn_go2": [
            46490,
            1942,
            false
        ],
        "lenn_go3": [
            48681,
            2842,
            false
        ],
        "lenn_go4": [
            51773,
            1447,
            false
        ],
        "lenn_go5": [
            53470,
            1278,
            false
        ],
        "lenn_go6": [
            54997,
            1059,
            false
        ],
        "lenn_go7": [
            56305,
            1732,
            false
        ],
        "lenn_horn1": [
            58287,
            1784,
            false
        ],
        "lenn_wp1": [
            60320,
            1649,
            false
        ],
        "lenn_wp2": [
            62219,
            1282,
            false
        ],
        "lenn_wp3": [
            63751,
            2166,
            false
        ],
        "lenn_wp4": [
            66166,
            1292,
            false
        ]
    }
};
var toniConfig = {
    "urls": [
        "toni_ogg-mit-mp3.opus",
        "audio/soundpacks/toni/toni.mp3",
        "toni_ogg-mit-mp3.webm"
    ],
    "sprite": {
        "toni_boo_1": [
            0,
            1379,
            false
        ],
        "toni_wp1": [
            1628,
            931,
            false
        ],
        "toni_hoo1": [
            2809,
            832,
            false
        ],
        "toni_go1": [
            3890,
            1844,
            false
        ],
        "toni_gg1": [
            5983,
            1066,
            false
        ],
        "toni_boo_2": [
            7298,
            1523,
            false
        ],
        "toni_boo_3": [
            9070,
            1587,
            false
        ],
        "toni_boo_4": [
            10906,
            1630,
            false
        ],
        "toni_boo_5": [
            12786,
            1639,
            false
        ],
        "toni_boo_6": [
            14675,
            1645,
            false
        ],
        "toni_boo_7": [
            16569,
            1598,
            false
        ],
        "toni_boo_8": [
            18417,
            1726,
            false
        ],
        "toni_clap10": [
            20392,
            95,
            false
        ],
        "toni_clap11": [
            20737,
            144,
            false
        ],
        "toni_clap12": [
            21130,
            70,
            false
        ],
        "toni_clap13": [
            21450,
            89,
            false
        ],
        "toni_clap14": [
            21788,
            112,
            false
        ],
        "toni_clap15": [
            22150,
            70,
            false
        ],
        "toni_clap16": [
            22470,
            70,
            false
        ],
        "toni_clap1": [
            22789,
            118,
            false
        ],
        "toni_clap2": [
            23157,
            144,
            false
        ],
        "toni_clap3": [
            23551,
            133,
            false
        ],
        "toni_clap4": [
            23933,
            47,
            false
        ],
        "toni_clap5": [
            24229,
            118,
            false
        ],
        "toni_clap6": [
            24597,
            47,
            false
        ],
        "toni_clap7": [
            24893,
            24,
            false
        ],
        "toni_clap8": [
            25166,
            24,
            false
        ],
        "toni_clap9": [
            25440,
            47,
            false
        ],
        "toni_cry1": [
            25736,
            1799,
            false
        ],
        "toni_cry2": [
            27784,
            1816,
            false
        ],
        "toni_drink1": [
            29850,
            1810,
            false
        ],
        "toni_hoo2": [
            31909,
            1685,
            false
        ],
        "toni_hoo3": [
            33844,
            1604,
            false
        ],
        "toni_hoo4": [
            35698,
            1604,
            false
        ],
        "toni_hoo5": [
            37552,
            1645,
            false
        ],
        "toni_hoo6": [
            39446,
            1569,
            false
        ],
        "toni_hoo8": [
            41265,
            1616,
            false
        ],
        "toni_horn1": [
            43130,
            1784,
            false
        ]
    }
};
var lionConfig = {
    "urls": [
        "lion.opus",
        "audio/soundpacks/lion/lion.mp3",
        "lion.webm"
    ],
    "sprite": {
        "lion_boo1": [
            0,
            1489,
            false
        ],
        "lion_boo2": [
            1738,
            955,
            false
        ],
        "lion_boo3": [
            2942,
            898,
            false
        ],
        "lion_boo4": [
            4089,
            1611,
            false
        ],
        "lion_boo5": [
            5949,
            1205,
            false
        ],
        "lion_boo6": [
            7403,
            886,
            false
        ],
        "lion_boo7": [
            8539,
            1142,
            false
        ],
        "lion_boo8": [
            9930,
            1692,
            false
        ],
        "lion_boo9": [
            11872,
            1472,
            false
        ],
        "lion_clap10": [
            13594,
            95,
            false
        ],
        "lion_clap11": [
            13938,
            144,
            false
        ],
        "lion_clap12": [
            14332,
            70,
            false
        ],
        "lion_clap13": [
            14651,
            89,
            false
        ],
        "lion_clap14": [
            14990,
            112,
            false
        ],
        "lion_clap15": [
            15352,
            70,
            false
        ],
        "lion_clap16": [
            15671,
            70,
            false
        ],
        "lion_clap1": [
            15991,
            118,
            false
        ],
        "lion_clap2": [
            16358,
            144,
            false
        ],
        "lion_clap3": [
            16752,
            133,
            false
        ],
        "lion_clap4": [
            17134,
            47,
            false
        ],
        "lion_clap5": [
            17431,
            118,
            false
        ],
        "lion_clap6": [
            17798,
            47,
            false
        ],
        "lion_clap7": [
            18095,
            24,
            false
        ],
        "lion_clap8": [
            18368,
            24,
            false
        ],
        "lion_clap9": [
            18641,
            47,
            false
        ],
        "lion_cry1": [
            18937,
            1783,
            false
        ],
        "lion_cry2": [
            20970,
            1625,
            false
        ],
        "lion_cry3": [
            22845,
            1613,
            false
        ],
        "lion_cry4": [
            24708,
            1375,
            false
        ],
        "lion_cry5": [
            26332,
            1652,
            false
        ],
        "lion_drink1": [
            28233,
            1810,
            false
        ],
        "lion_gg10": [
            30293,
            1141,
            false
        ],
        "lion_gg11": [
            31683,
            1324,
            false
        ],
        "lion_gg12": [
            33257,
            983,
            false
        ],
        "lion_gg13": [
            34490,
            1105,
            false
        ],
        "lion_gg14": [
            35844,
            977,
            false
        ],
        "lion_gg1": [
            37071,
            1232,
            false
        ],
        "lion_gg2": [
            38552,
            1309,
            false
        ],
        "lion_gg3": [
            40111,
            1190,
            false
        ],
        "lion_gg4": [
            41550,
            1118,
            false
        ],
        "lion_gg5": [
            42918,
            1254,
            false
        ],
        "lion_gg6": [
            44421,
            1256,
            false
        ],
        "lion_gg7": [
            45927,
            647,
            false
        ],
        "lion_gg8": [
            46824,
            1039,
            false
        ],
        "lion_gg9": [
            48113,
            667,
            false
        ],
        "lion_hoo10": [
            49029,
            1189,
            false
        ],
        "lion_hoo1": [
            50467,
            1739,
            false
        ],
        "lion_hoo2": [
            52456,
            1491,
            false
        ],
        "lion_hoo3": [
            54197,
            1236,
            false
        ],
        "lion_hoo4": [
            55682,
            1425,
            false
        ],
        "lion_hoo5": [
            57356,
            2026,
            false
        ],
        "lion_hoo6": [
            59632,
            1208,
            false
        ],
        "lion_hoo7": [
            61089,
            1360,
            false
        ],
        "lion_hoo8": [
            62699,
            1665,
            false
        ],
        "lion_hoo9": [
            64614,
            895,
            false
        ],
        "lion_horn1": [
            65758,
            1784,
            false
        ],
        "lion_wp1": [
            67792,
            1421,
            false
        ],
        "lion_wp2": [
            69462,
            809,
            false
        ],
        "lion_wp3": [
            70520,
            1999,
            false
        ]
    }
};

var combinedConfig = {"urls":["audio/newSoundsCombined/allCombined.mp3"],"sprite":{"action_active_blowhorn_1":[0,750],"action_active_blowhorn_2":[2000,1062.4943310657598],"action_active_blowhorn_3":[5000,1906.757369614512],"action_active_blowhorn_4":[8000,1833.333333333334],"action_active_clap_1":[11000,562.4943310657588],"action_active_clap_2.1":[13000,76.03174603174523],"action_active_clap_2.2":[15000,76.03174603174523],"action_active_clap_2.3":[17000,76.03174603174523],"action_active_clap_3.1":[19000,261.4512471655317],"action_active_clap_3.2":[21000,261.4512471655317],"action_active_clap_3.3":[23000,261.4512471655317],"action_active_clap_3.4":[25000,261.4512471655317],"action_active_clap_3.5":[27000,261.4512471655317],"action_active_raisesign_1":[29000,500],"action_active_raisesign_2":[31000,512.49433106576],"action_passive_despawn":[33000,617.6870748299308],"action_passive_spawn":[35000,833.3333333333358],"voice_female_boo_3":[37000,1374.9886621315213],"voice_female_boo_4":[40000,1374.9886621315213],"voice_female_cheer_3":[43000,714.0589569160979],"voice_female_cheer_5":[45000,645.8276643990928],"voice_female_cry_2":[47000,2000],"voice_female_cry_4":[50000,567.6870748299336],"voice_female_drink_6":[52000,1083.3333333333358],"voice_female_gg_4":[55000,604.6712018140568],"voice_female_gg_5":[57000,708.843537414964],"voice_female_gg_7":[59000,562.494331065757],"voice_female_gg_8":[61000,770.8163265306141],"voice_female_laugh_10":[63000,1166.6666666666715],"voice_female_laugh_7":[66000,805.7142857142878],"voice_female_laugh_8":[68000,708.321995464857],"voice_female_laugh_9":[70000,859.8866213151979],"voice_female_snore_2":[72000,874.9886621315142],"voice_female_snore_3":[74000,1458.321995464857],"voice_female_wellplayed_1":[77000,656.7573696145104],"voice_female_wellplayed_2":[79000,624.9886621315142],"voice_female_wellplayed_3":[81000,666.6666666666714],"voice_female_yawn_1":[83000,1541.6553287981856],"voice_female_yawn_2":[86000,1187.4829931972783],"voice_male_boo_1":[89000,1302.585034013603],"voice_male_boo_3":[92000,1145.8276643990928],"voice_male_cheer_3":[95000,769.2743764172292],"voice_male_cheer_4":[97000,624.9886621315142],"voice_male_cry":[99000,895.8276643990928],"voice_male_cry_4":[101000,1000],"voice_male_drink_6":[103000,1083.3333333333285],"voice_male_gg_1":[106000,770.816326530607],"voice_male_gg_2":[108000,720.2947845805028],"voice_male_gg_4":[110000,594.7845804988674],"voice_male_laugh_5":[112000,750],"voice_male_laugh_6":[114000,791.6553287981856],"voice_male_snore_1":[116000,1187.4829931972783],"voice_male_snore_3":[119000,874.9886621315142],"voice_male_wellplayed_1":[121000,791.6553287981856],"voice_male_wellplayed_3":[123000,908.3219954648456],"voice_male_wellplayed_4":[125000,916.6666666666714],"voice_male_yawn":[127000,1479.1609977324356]}};

var backgroundConfig = {
    "urls": {
        veryPositive: [
            "audio/backgroundSound/extremly_positive_jubel.mp3"
        ],
        positive: [
            "audio/backgroundSound/positive_jubel_komplett.mp3"
        ],
        neutral: [
            "audio/bg_atmo_1.mp3"
        ],
        negative: [
            "audio/backgroundSound/negative.mp3"
        ]
    }
};

var avatarSound = new Howl({
    src: [toniConfig.urls[1]],
    sprite: toniConfig.sprite,
    volume: 0.5
});

var otherAvatarsSound = new Howl({
    src: [combinedConfig.urls[0]],
    sprite: combinedConfig.sprite,
    volume: 0.5
});

//var systemSound = new Howl({ //there aren't any yet
//    src: [lennConfig.urls[1]],
//    sprite: lennConfig.sprite,
//    volume: 0.5
//});

var backgroundSoundVeryPositive = new Howl({ //TODO stream them in (not possible atm, cause we got only one?)
    src: [backgroundConfig.urls.veryPositive[0]],
    autoplay: true,
    loop: true,
    volume: 0.5
});

var backgroundSoundPositive = new Howl({ //TODO stream them in (not possible atm, cause we got only one?)
    src: [backgroundConfig.urls.positive[0]],
    autoplay: true,
    loop: true,
    volume: 0.5
});

var backgroundSoundNeutral = new Howl({ //TODO stream them in (not possible atm, cause we got only one?)
    src: [backgroundConfig.urls.neutral[0]],
    autoplay: true,
    loop: true,
    volume: 0.5
});

var backgroundSoundNegative = new Howl({ //TODO stream them in (not possible atm, cause we got only one?)
    src: [backgroundConfig.urls.negative[0]],
    autoplay: true,
    loop: true,
    volume: 0.5
});

////////////////Twitch Stream ////////////////
var player;

function startTwitchStream() {
    var url = $("#twitchUrl").val();
    if(url !== "") {
        if(typeof player === "undefined") {
            var options = {
                width: 400,
                height: 300,
                channel: url
                //video: "{VIDEO_ID}"
            };
            player = new Twitch.Player("twitchFrame", options);
            player.setVolume(0.5);
            player.addEventListener(Twitch.Player.PAUSE, function (event) {
                console.log('Player is paused!');
            });
        } else {
            player.setChannel(url);
        }
    }
}


//otherAvatarsSound.play("lenn_wp4");
//avatarSound.play("toni_wp1");

//sound.play();
//var count = 0;

// Adds event listeners (simulates the Server Events)
document.addEventListener("ownAvatar", function(event) {
    console.log("ownAvatar serverEvent recieved!", event);
    avatarSound.play(event.detail);
});

document.addEventListener("otherAvatars", function(event) {
    console.log("otherAvatars serverEvent recieved!", event);
    otherAvatarsSound.play(event.detail);
});




function changeVolumeOwn() {
    var volume = $( "#ownAvatarVolumeSlider" ).slider( "value" ) / 100;
    avatarSound.volume(volume);
    console.log(volume);
}

function changeVolumeOthers() {
    var volume = $( "#otherAvatarsVolumeSlider" ).slider( "value" ) / 100;
    otherAvatarsSound.volume(volume);
    console.log(volume);
}

function changeVolumeSystem() {
    var volume = $( "#systemSoundVolumeSlider" ).slider( "value" ) / 100;
    systemSound.volume(volume);
    console.log(volume);
}

function changeVolumeBackgroundVeryPositive() {
    var volume = $( "#backgroundSoundVeryPositiveVolumeSlider" ).slider( "value" ) / 100;
    backgroundSoundVeryPositive.volume(volume);
    console.log(volume);
}

function changeVolumeBackgroundPositive() {
    var volume = $( "#backgroundSoundPositiveVolumeSlider" ).slider( "value" ) / 100;
    backgroundSoundPositive.volume(volume);
    console.log(volume);
}

function changeVolumeBackgroundNeutral() {
    var volume = $( "#backgroundSoundNeutralVolumeSlider" ).slider( "value" ) / 100;
    backgroundSoundNeutral.volume(volume);
    console.log(volume);
}

function changeVolumeBackgroundNegative() {
    var volume = $( "#backgroundSoundNegativeVolumeSlider" ).slider( "value" ) / 100;
    backgroundSoundNegative.volume(volume);
    console.log(volume);
}

function changeVolumeMaster() {
    var volume = $( "#masterVolumeSlider" ).slider( "value" ) / 100;
    Howler.volume(volume);
    console.log(volume);
}


//////////SLIDER///////////////////

$( function() {
    $( "#ownAvatarVolumeSlider" ).slider({
        value: avatarSound.volume() * 100,
        max: 100,
        slide: changeVolumeOwn,
        change: changeVolumeOwn
    });
    $( "#otherAvatarsVolumeSlider" ).slider({
        value: otherAvatarsSound.volume() * 100,
        max: 100,
        slide: changeVolumeOthers,
        change: changeVolumeOthers
    });
    //$( "#systemSoundVolumeSlider" ).slider({
    //    value: systemSound.volume() * 100,
    //    max: 100,
    //    slide: changeVolumeSystem,
    //    change: changeVolumeSystem
    //});
    $( "#backgroundSoundVeryPositiveVolumeSlider" ).slider({
        value: backgroundSoundPositive.volume() * 100,
        max: 100,
        slide: changeVolumeBackgroundVeryPositive,
        change: changeVolumeBackgroundVeryPositive
    });
    $( "#backgroundSoundPositiveVolumeSlider" ).slider({
        value: backgroundSoundPositive.volume() * 100,
        max: 100,
        slide: changeVolumeBackgroundPositive,
        change: changeVolumeBackgroundPositive
    });
    $( "#backgroundSoundNeutralVolumeSlider" ).slider({
        value: backgroundSoundNeutral.volume() * 100,
        max: 100,
        slide: changeVolumeBackgroundNeutral,
        change: changeVolumeBackgroundNeutral
    });
    $( "#backgroundSoundNegativeVolumeSlider" ).slider({
        value: backgroundSoundNegative.volume() * 100,
        max: 100,
        slide: changeVolumeBackgroundNegative,
        change: changeVolumeBackgroundNegative
    });
    $( "#masterVolumeSlider" ).slider({
        value: Howler.volume() * 100,
        max: 100,
        slide: changeVolumeMaster,
        change: changeVolumeMaster
    });
} );


//var initialized = {};

//function changeVolume(type, id) {
//    if(!initialized[id]) {
//        console.log(id, initialized[id]);
//        return;
//    }
//    var volume = $( id ).slider( "value" ) / 100;
//    if(type === "own") {
//        avatarSound.volume(volume);
//    } else if(type === "others") {
//        otherAvatarsSound.volume(volume);
//    } else if(type === "system") {
//        systemSound.volume(volume);
//    } else if(type === "background") {
//        backgroundSound.volume(volume);
//    } else if(type === "master") {
//        Howler.volume(volume);
//    }
//    console.log(type, volume);
//}
