/**
 * Created by tobi on 05.10.16.
 */


var lennConfig = {
    "urls": [
        "lenn.opus",
        "audio/soundpacks/lenn/lenn.mp3",
        "lenn.webm"
    ],
    "sprite": {
        "lenn_boo10": [
            0,
            1556,
            false
        ],
        "lenn_boo11": [
            1805,
            791,
            false
        ],
        "lenn_boo12": [
            2846,
            1508,
            false
        ],
        "lenn_boo13": [
            4603,
            1496,
            false
        ],
        "lenn_boo14": [
            6349,
            1091,
            false
        ],
        "lenn_boo15": [
            7689,
            1455,
            false
        ],
        "lenn_boo16": [
            9393,
            1176,
            false
        ],
        "lenn_boo17": [
            10819,
            840,
            false
        ],
        "lenn_boo1": [
            11908,
            1653,
            false
        ],
        "lenn_boo2": [
            13811,
            1673,
            false
        ],
        "lenn_boo3": [
            15734,
            1033,
            false
        ],
        "lenn_boo4": [
            17016,
            1542,
            false
        ],
        "lenn_boo5": [
            18807,
            1399,
            false
        ],
        "lenn_boo6": [
            20455,
            1370,
            false
        ],
        "lenn_boo7": [
            22075,
            544,
            false
        ],
        "lenn_boo8": [
            22868,
            1247,
            false
        ],
        "lenn_boo9": [
            24365,
            1155,
            false
        ],
        "lenn_clap10": [
            25769,
            95,
            false
        ],
        "lenn_clap11": [
            26114,
            144,
            false
        ],
        "lenn_clap12": [
            26507,
            70,
            false
        ],
        "lenn_clap13": [
            26827,
            89,
            false
        ],
        "lenn_clap14": [
            27166,
            112,
            false
        ],
        "lenn_clap15": [
            27527,
            70,
            false
        ],
        "lenn_clap16": [
            27847,
            70,
            false
        ],
        "lenn_clap1": [
            28167,
            118,
            false
        ],
        "lenn_clap2": [
            28534,
            144,
            false
        ],
        "lenn_clap3": [
            28928,
            133,
            false
        ],
        "lenn_clap4": [
            29310,
            47,
            false
        ],
        "lenn_clap5": [
            29606,
            118,
            false
        ],
        "lenn_clap6": [
            29974,
            47,
            false
        ],
        "lenn_clap7": [
            30270,
            24,
            false
        ],
        "lenn_clap8": [
            30544,
            24,
            false
        ],
        "lenn_clap9": [
            30817,
            47,
            false
        ],
        "lenn_cry1": [
            31113,
            3618,
            false
        ],
        "lenn_cry2": [
            34980,
            2085,
            false
        ],
        "lenn_drink1": [
            37315,
            1810,
            false
        ],
        "lenn_gg1": [
            39375,
            1254,
            false
        ],
        "lenn_gg2": [
            40879,
            1675,
            false
        ],
        "lenn_gg3": [
            42803,
            1094,
            false
        ],
        "lenn_go1": [
            44146,
            2094,
            false
        ],
        "lenn_go2": [
            46490,
            1942,
            false
        ],
        "lenn_go3": [
            48681,
            2842,
            false
        ],
        "lenn_go4": [
            51773,
            1447,
            false
        ],
        "lenn_go5": [
            53470,
            1278,
            false
        ],
        "lenn_go6": [
            54997,
            1059,
            false
        ],
        "lenn_go7": [
            56305,
            1732,
            false
        ],
        "lenn_horn1": [
            58287,
            1784,
            false
        ],
        "lenn_wp1": [
            60320,
            1649,
            false
        ],
        "lenn_wp2": [
            62219,
            1282,
            false
        ],
        "lenn_wp3": [
            63751,
            2166,
            false
        ],
        "lenn_wp4": [
            66166,
            1292,
            false
        ]
    }
};
var lennNumOfSprites = Object.keys(lennConfig.sprite).length;
var toniConfig = {
    "urls": [
        "toni_ogg-mit-mp3.opus",
        "audio/soundpacks/toni/toni.mp3",
        "toni_ogg-mit-mp3.webm"
    ],
    "sprite": {
        "toni_boo_1": [
            0,
            1379,
            false
        ],
        "toni_wp1": [
            1628,
            931,
            false
        ],
        "toni_hoo1": [
            2809,
            832,
            false
        ],
        "toni_go1": [
            3890,
            1844,
            false
        ],
        "toni_gg1": [
            5983,
            1066,
            false
        ],
        "toni_boo_2": [
            7298,
            1523,
            false
        ],
        "toni_boo_3": [
            9070,
            1587,
            false
        ],
        "toni_boo_4": [
            10906,
            1630,
            false
        ],
        "toni_boo_5": [
            12786,
            1639,
            false
        ],
        "toni_boo_6": [
            14675,
            1645,
            false
        ],
        "toni_boo_7": [
            16569,
            1598,
            false
        ],
        "toni_boo_8": [
            18417,
            1726,
            false
        ],
        "toni_clap10": [
            20392,
            95,
            false
        ],
        "toni_clap11": [
            20737,
            144,
            false
        ],
        "toni_clap12": [
            21130,
            70,
            false
        ],
        "toni_clap13": [
            21450,
            89,
            false
        ],
        "toni_clap14": [
            21788,
            112,
            false
        ],
        "toni_clap15": [
            22150,
            70,
            false
        ],
        "toni_clap16": [
            22470,
            70,
            false
        ],
        "toni_clap1": [
            22789,
            118,
            false
        ],
        "toni_clap2": [
            23157,
            144,
            false
        ],
        "toni_clap3": [
            23551,
            133,
            false
        ],
        "toni_clap4": [
            23933,
            47,
            false
        ],
        "toni_clap5": [
            24229,
            118,
            false
        ],
        "toni_clap6": [
            24597,
            47,
            false
        ],
        "toni_clap7": [
            24893,
            24,
            false
        ],
        "toni_clap8": [
            25166,
            24,
            false
        ],
        "toni_clap9": [
            25440,
            47,
            false
        ],
        "toni_cry1": [
            25736,
            1799,
            false
        ],
        "toni_cry2": [
            27784,
            1816,
            false
        ],
        "toni_drink1": [
            29850,
            1810,
            false
        ],
        "toni_hoo2": [
            31909,
            1685,
            false
        ],
        "toni_hoo3": [
            33844,
            1604,
            false
        ],
        "toni_hoo4": [
            35698,
            1604,
            false
        ],
        "toni_hoo5": [
            37552,
            1645,
            false
        ],
        "toni_hoo6": [
            39446,
            1569,
            false
        ],
        "toni_hoo8": [
            41265,
            1616,
            false
        ],
        "toni_horn1": [
            43130,
            1784,
            false
        ]
    }
};
var toniNumOfSprites = Object.keys(toniConfig.sprite).length;

//var femaleConfig = {"urls":["female.mp3"],"sprite":{"voice_female_boo_3":[0,1374.9886621315193],"voice_female_boo_4":[3000,1374.9886621315195],"voice_female_cheer_3":[6000,714.0589569160998],"voice_female_cheer_5":[8000,645.8276643990928],"voice_female_cry_2":[10000,2000],"voice_female_cry_4":[13000,567.6870748299318],"voice_female_drink_6":[15000,1083.3333333333321],"voice_female_gg_4":[18000,604.6712018140603],"voice_female_gg_5":[20000,708.8435374149675],"voice_female_gg_7":[22000,562.4943310657607],"voice_female_gg_8":[24000,770.8163265306141],"voice_female_laugh_10":[26000,1166.6666666666679],"voice_female_laugh_7":[29000,805.7142857142843],"voice_female_laugh_8":[31000,708.3219954648534],"voice_female_laugh_9":[33000,859.8866213151907],"voice_female_snore_2":[35000,874.9886621315213],"voice_female_snore_3":[37000,1458.3219954648498],"voice_female_wellplayed_1":[40000,656.7573696145104],"voice_female_wellplayed_2":[42000,624.9886621315213],"voice_female_wellplayed_3":[44000,666.6666666666642],"voice_female_yawn_1":[46000,1541.6553287981856],"voice_female_yawn_2":[49000,1187.4829931972783]}};
//var femaleNumOfSprites = Object.keys(femaleConfig.sprite).length;
//var maleConfig = {"urls":["male.mp3"],"sprite":{"voice_male_boo_1":[0,1302.5850340136053],"voice_male_boo_3":[3000,1145.8276643990928],"voice_male_cheer_3":[6000,769.2743764172336],"voice_male_cheer_4":[8000,624.9886621315195],"voice_male_cry":[10000,895.8276643990928],"voice_male_cry_4":[12000,1000],"voice_male_drink_6":[14000,1083.333333333334],"voice_male_gg_1":[17000,770.8163265306141],"voice_male_gg_2":[19000,720.2947845804993],"voice_male_gg_4":[21000,594.7845804988674],"voice_male_laugh_5":[23000,750],"voice_male_laugh_6":[25000,791.6553287981856],"voice_male_snore_1":[27000,1187.4829931972783],"voice_male_snore_3":[30000,874.9886621315177],"voice_male_wellplayed_1":[32000,791.6553287981856],"voice_male_wellplayed_3":[34000,908.3219954648528],"voice_male_wellplayed_4":[36000,916.6666666666642],"voice_male_yawn":[38000,1479.1609977324285]}};
//var maleNumOfSprites = Object.keys(maleConfig.sprite).length;
//var nonvoiceConfig = {"urls":["nonvoice.mp3"],"sprite":{"action_active_blowhorn_1":[0,750],"action_active_blowhorn_2":[2000,1062.4943310657598],"action_active_blowhorn_3":[5000,1906.757369614512],"action_active_blowhorn_4":[8000,1833.333333333334],"action_active_clap_1":[11000,562.4943310657588],"action_active_clap_2.1":[13000,76.03174603174523],"action_active_clap_2.2":[15000,76.03174603174523],"action_active_clap_2.3":[17000,76.03174603174523],"action_active_clap_3.1":[19000,261.4512471655317],"action_active_clap_3.2":[21000,261.4512471655317],"action_active_clap_3.3":[23000,261.4512471655317],"action_active_clap_3.4":[25000,261.4512471655317],"action_active_clap_3.5":[27000,261.4512471655317],"action_active_raisesign_1":[29000,500],"action_active_raisesign_2":[31000,512.49433106576],"action_passive_despawn":[33000,617.6870748299308],"action_passive_spawn":[35000,833.3333333333358]}};
//var nonvoiceNumOfSprites = Object.keys(nonvoiceConfig.sprite).length;

var combinedConfig = {"urls":["audio/newSoundsCombined/allCombined.mp3"],"sprite":{"action_active_blowhorn_1":[0,750],"action_active_blowhorn_2":[2000,1062.4943310657598],"action_active_blowhorn_3":[5000,1906.757369614512],"action_active_blowhorn_4":[8000,1833.333333333334],"action_active_clap_1":[11000,562.4943310657588],"action_active_clap_2.1":[13000,76.03174603174523],"action_active_clap_2.2":[15000,76.03174603174523],"action_active_clap_2.3":[17000,76.03174603174523],"action_active_clap_3.1":[19000,261.4512471655317],"action_active_clap_3.2":[21000,261.4512471655317],"action_active_clap_3.3":[23000,261.4512471655317],"action_active_clap_3.4":[25000,261.4512471655317],"action_active_clap_3.5":[27000,261.4512471655317],"action_active_raisesign_1":[29000,500],"action_active_raisesign_2":[31000,512.49433106576],"action_passive_despawn":[33000,617.6870748299308],"action_passive_spawn":[35000,833.3333333333358],"voice_female_boo_3":[37000,1374.9886621315213],"voice_female_boo_4":[40000,1374.9886621315213],"voice_female_cheer_3":[43000,714.0589569160979],"voice_female_cheer_5":[45000,645.8276643990928],"voice_female_cry_2":[47000,2000],"voice_female_cry_4":[50000,567.6870748299336],"voice_female_drink_6":[52000,1083.3333333333358],"voice_female_gg_4":[55000,604.6712018140568],"voice_female_gg_5":[57000,708.843537414964],"voice_female_gg_7":[59000,562.494331065757],"voice_female_gg_8":[61000,770.8163265306141],"voice_female_laugh_10":[63000,1166.6666666666715],"voice_female_laugh_7":[66000,805.7142857142878],"voice_female_laugh_8":[68000,708.321995464857],"voice_female_laugh_9":[70000,859.8866213151979],"voice_female_snore_2":[72000,874.9886621315142],"voice_female_snore_3":[74000,1458.321995464857],"voice_female_wellplayed_1":[77000,656.7573696145104],"voice_female_wellplayed_2":[79000,624.9886621315142],"voice_female_wellplayed_3":[81000,666.6666666666714],"voice_female_yawn_1":[83000,1541.6553287981856],"voice_female_yawn_2":[86000,1187.4829931972783],"voice_male_boo_1":[89000,1302.585034013603],"voice_male_boo_3":[92000,1145.8276643990928],"voice_male_cheer_3":[95000,769.2743764172292],"voice_male_cheer_4":[97000,624.9886621315142],"voice_male_cry":[99000,895.8276643990928],"voice_male_cry_4":[101000,1000],"voice_male_drink_6":[103000,1083.3333333333285],"voice_male_gg_1":[106000,770.816326530607],"voice_male_gg_2":[108000,720.2947845805028],"voice_male_gg_4":[110000,594.7845804988674],"voice_male_laugh_5":[112000,750],"voice_male_laugh_6":[114000,791.6553287981856],"voice_male_snore_1":[116000,1187.4829931972783],"voice_male_snore_3":[119000,874.9886621315142],"voice_male_wellplayed_1":[121000,791.6553287981856],"voice_male_wellplayed_3":[123000,908.3219954648456],"voice_male_wellplayed_4":[125000,916.6666666666714],"voice_male_yawn":[127000,1479.1609977324356]}};
var combinedLength = Object.keys(combinedConfig.sprite).length;

function emitAvatarSoundEvents() {
    //var chooseVoiceType = randomIntFromInterval(1, 3) - 1; //choose female, male, or nonvoice

    var rndNumber = randomIntFromInterval(1, combinedLength) - 1; //need a random index
    var rndSprite = key(combinedConfig.sprite, rndNumber);

    // Create the event
    var event = new CustomEvent("otherAvatars", { "detail": rndSprite });

    // Dispatch/Trigger/Fire the event
    document.dispatchEvent(event);
}

function emitOwnAvatarEvents() {
    var rndNumber = randomIntFromInterval(1, toniNumOfSprites) - 1; //need a random index
    var rndSprite = key(toniConfig.sprite, rndNumber);

    // Create the event
    var event = new CustomEvent("ownAvatar", { "detail": rndSprite });

    // Dispatch/Trigger/Fire the event
    document.dispatchEvent(event);
}



//INIT
var numOfAvatars = 30;
var intervalMinMS = 4000;
var intervalMaxMS = 10000;
var stopper;
var started = false;

$("#numOfAvatars_input").val(numOfAvatars);
$("#intervalMinMS_input").val(intervalMinMS);
$("#intervalMaxMS_input").val(intervalMaxMS);

function startAllLoops() {
    stopper = false;
    for(var i = 0; i < numOfAvatars; i++) {
        loopOtherAvatars();
        console.log(i);
    }
}

function stopActions() {
    stopper = true;
    setTimeout(function () {
        //just w8 till all are stopped
    }, intervalMaxMS);
}

function startActions() {
    //stopActions();

    numOfAvatars = $("#numOfAvatars_input").val();
    intervalMinMS = $("#intervalMinMS_input").val();
    intervalMaxMS = $("#intervalMaxMS_input").val();

    startAllLoops();
}


function loopOtherAvatars() { //loops with given time and emits events
    var rand = randomIntFromInterval(intervalMinMS, intervalMaxMS);
    console.log(intervalMaxMS, intervalMinMS, rand);
    if(!stopper) {
        setTimeout(function () {
            emitAvatarSoundEvents();
            loopOtherAvatars();
        }, rand);
    }
}

//(function loopOwnAvatar() { //loops with random time between 1s and 4s and emits events
//    var rand = Math.round(Math.random() * 3000) + 1000;
//    setTimeout(function() {
//        emitOwnAvatarEvents();
//        loopOwnAvatar();
//    }, rand);
//}());



//console.log(randomIntFromInterval(intervalMinMS, intervalMaxMS));
//console.log(Math.floor((1500-500+1)+500));



////////////////HELPER/////////////////////
function randomIntFromInterval(min,max)
{
    return Math.floor((Math.random()*(max-min+1))+parseInt(min));
}

//functionHelper to get the sprite from Object by Index
var object = {
    key: function(n) {
        return Object.keys(this)[n];
    }
};

function key(obj, idx) {
    return object.key.call(obj, idx);
}
