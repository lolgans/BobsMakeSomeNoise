/**
 * Created by tobi on 10.10.16.
 */

/**
 * Trying to generate one big sound file (+ corresponding JSON) from all sounds.
 *
 *
 * Description:
 *
 *
*/

var audiosprite = require('audiosprite');
var fs = require('fs');
var files;

var audioOptions = {
    output: 'default',
    format: 'howler',
    export: 'wav'
};

var argv = require('yargs') // TODO add this later
    .usage('to start spriteCreation, do: node spriteCreation.js <Options>')
    .option('h', {
        alias: 'httpPort',
        describe: 'Set the port for the api to listen on.',
        nargs: 1,
        type: 'number'
    })
    .option('s', {
        alias: 'redisServer',
        describe: 'defines the full redis url',
        type: 'string',
        nargs: 1,
        default: typeof process.env.AMB_REDIS_URL !== "undefined" ? process.env.AMB_REDIS_URL.replace('tcp', 'redis') : 'redis://localhost:6379'
    })
    .help('?')
    .alias('?', 'help')
    .argv;

//var gameConfig = require('./gameConfig/game-config.json');



fs.readdir('sounds_matt_selection_for_tobi/all', function(err, fileArray) {
    if(err) {
        console.log("Error reading files:", err);
        //process.exit(-1);
    }

    for(var i = 0; i < fileArray.length; i++) {
        fileArray[i] = 'sounds_matt_selection_for_tobi/all/' + fileArray[i];
    }

    console.log(fileArray);
    files = fileArray;


    if(typeof files !== "undefined") {
        audioOptions.output = 'avatarSounds';


        audiosprite(files, audioOptions, function(err, obj) {
            if (err) console.error(err);

            console.log(JSON.stringify(obj, null, 2));

            writeJsonToFile(JSON.stringify(obj, null, 4), "result/allCombined.json");


        });
    }
});

//var files = ['file1.mp3', 'file2.mp3'];

//var game-config


function writeJsonToFile(jsonTxt, fileName) {
    fs.writeFile(fileName, jsonTxt, function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });
}


